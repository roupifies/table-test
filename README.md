# Table data
## How to run
1. go to the root of the project
2. run `docker-compose up`
*OR*
2. 
    - run `./gradlew bootRun` to run backend on java 8
    - run `npm run install` & `npm run client` to run frontend on node 10
    
## Structure
### Backend 
it uses `SpringBootJspApplication` and fetches, manipulates and expose data in a restful way. 
it fetches all the data and then do sorting, pagination, and searching.
`TableService` interface abstract the implementation to allow controller replacing it in future by following `open-close` principal.
I have tried to keep classes small and single responsible.

### assumptions
- As some of the `createdOn` dates were in epoch and some were in the string and some missing,
I have decided to convert all of them to the short date format `dd/MM/YYYY` to allow user easily order them.
- The backend expose data into a *JSP* pages in a JSON format to meet the requirement. 
- the outbound and domain responses, separated to have an anti-corruption layer.

### front end
it uses `React` with `redux` to make a call to the backend in an async way.
on startup `componentDidMount` raise `getResult` action and fetch data from backend with an async call. 
Once the data is ready it passes the data to the store/reducer and let the content component knows about the updated result.

### assumptions
- User can paginate through data. 
- pagination reset when order by select changes. 
- pagination only available when no filter or search selected.
- user can order by id, name, createdOn. by default data ordered by id.
- user happy to see createdOn dates in `dd/MM/YYYY` format.
- user can search by name and it would show all name *contains* the query.
- user can filter by status

## requirements checklist

- Your task is to retrieve data from Google Cloud Storage (GCS) and display it in a table. 
    - backend call the GCS and front-end showing it. 
    
- Please complete this in a JSP Page.
    - backend uses `SpringBootJspApplication` and expose data into `JSP` pages as a json response.
    
- The table should have the following columns: id, name, status, description, delta, createdOn
    - done
    
*The table should have the following features:*
- Free text search by "name"
    - user can search services that contain requested query anywhere in the name.
- Filter by "status" value
    - user can filter result by "complete", "error", and "canceled", if no filter selected it will show all results. 
- Pagination, 20 entries per page
    - user can paginate through data on different selection of order. each page shows 20 results. 
    - the previous button is disabled when a user on the first page and the next button is disabled when the user on page 24 to prevent any user error.
- Row ordering on "id", "name", "createdOn"
    - done.
- The API call to GCS should be done on the backend, as well as all the data manipulations
    - All calls such as search, ordering, filter, and pagination done in the backend.
    - All data manipulation such as handling dates formats done in the backend.

- The frontend should do a REST API call to the backend to get the already transformed data and display it in the table.
    - done.
    
 - There are no specific requirements on the frameworks used, though Typescript / Javascript is appreciated.
    - I have uses React, javascript to meet job description requirement.
    
 - Docker is available if you want to use it.
    - docker-compose added to the project to allow user run it with simply running the `docker-compose up` command on any environment.
                          




   
