import { LOAD_RESULT } from './Type';

const initialState = {
  data: [{ id: 'loading', name: 'data ...'}],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_RESULT:
      return {
        ...state,
        data: action.data,
      };
    default:
      return state;
  }
};
