import SearchResultReducer from './SearchResultReducer';
import {LOAD_RESULT} from "./Type";

describe('search result reducer', () => {
  let testSubject;

  it('return init status when action is not defined', () => {
    const someInitState = {};
    const notDefinedAction = { type: undefined };
    testSubject = SearchResultReducer(someInitState, notDefinedAction);

    expect(testSubject).toEqual(someInitState);
  });

  it('add action data when action type is LOAD_RESULT', () => {
    const someInitState = {};
    const someData = 'some data';
    const notDefinedAction = { type: LOAD_RESULT, data: someData };
    testSubject = SearchResultReducer(someInitState, notDefinedAction);

    expect(testSubject).toEqual({ data: someData });
  });

});
