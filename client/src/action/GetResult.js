import {LOAD_RESULT} from '../reducer/Type';

const getResult = (pageNumber, orderBy) => (dispatch) => {
    orderBy = orderBy || 'id';
    pageNumber = pageNumber || '0';
    fetch('http://localhost:8080/data?orderBy='.concat(orderBy).concat('&pageNumber=').concat(pageNumber))
        .then(response => response.json())
        .then((data) => {
            dispatch({
                type: LOAD_RESULT,
                data: data['output'],
            });
        })
        .catch(() => {
        });
};

export default getResult;
