import {LOAD_RESULT} from '../reducer/Type';

const getResult = (status, name) => (dispatch) => {
    status = status || '';
    name = name || '';
    console.log(status,'stassstus');
    fetch('http://localhost:8080/search?name='.concat(name).concat('&status=').concat(status))
        .then(response => response.json())
        .then((data) => {
            dispatch({
                type: LOAD_RESULT,
                data: data['output'],
            });
        })
        .catch(() => {
        });
};

export default getResult;
