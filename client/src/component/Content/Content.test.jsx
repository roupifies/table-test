import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import {expect} from '../../service/chai';

import Content from './Content';

describe('Content', () => {
    test('should match snapshot', () => {
        expect(toJson(shallow(<Content/>))).toMatchSnapshot();
    });
});
