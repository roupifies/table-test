import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import getResult from '../../action/GetResult';
import getSearchResult from '../../action/GetSearchResult';
import './Content.css';
import OrderBy from "../OrderBy/OrderBy";
import FilterStatus from "../FilterStatus/FilterStatus";
import SearchByName from "../SearchByName/SearchByName";
import Pagination from "../Pagination/Pagination";

class Content extends PureComponent {
    constructor(props) {
        super(props);
        this.fetchData = this.fetchData.bind(this);
        this.searchData = this.searchData.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.prevPage = this.prevPage.bind(this);
        this.state = {pageNumber: 0, orderBy: 'id'}
    }

    componentDidMount() {
        this.props.getResult();
    }

    nextPage() {
        let pageNumber = this.state.pageNumber + 1;
        this.setState({pageNumber: pageNumber});
        this.props.getResult(pageNumber, this.state.orderBy);
    }

    prevPage() {
        let pageNumber = this.state.pageNumber - 1;
        this.setState({pageNumber: pageNumber});
        this.props.getResult(pageNumber, this.state.orderBy);
    }

    fetchData(event) {
        event.preventDefault();
        let requestedOrderBy = event.target.elements.orderBy;
        if (requestedOrderBy) {
            let orderBy = requestedOrderBy.value;
            this.setState({orderBy: orderBy, pageNumber: 0});
            this.props.getResult(0, orderBy)
        }
    }

    searchData(event) {
        event.preventDefault();
        let elements = event.target.elements;
        let requestedFilterByStatus = elements.filterByStatus || {};
        let filterByStatus = requestedFilterByStatus.value || "";

        let requestedSearchByName = elements.textSearch || {};
        let searchByName = requestedSearchByName.value || "";

        this.props.getSearchResult(filterByStatus, searchByName);
    }

    render() {
        const {data} = this.props;
        return (
            <div>
                <table className="content">
                    <thead>
                    <SearchByName searchData={this.searchData}/>
                    <FilterStatus searchData={this.searchData}/>
                    <OrderBy fetchData={this.fetchData}/>
                    <Pagination nextPage={this.nextPage} prevPage={this.prevPage}
                                pageNumber={this.state.pageNumber}/>
                    </thead>
                </table>
                <table className="content">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>status</th>
                        <th>description</th>
                        <th>delta</th>
                        <th>createdOn</th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.map((eachData, index) =>
                        <tr key={index}>
                            <td>{eachData.id}</td>
                            <td>{eachData.name}</td>
                            <td>{eachData.status}</td>
                            <td>{eachData.description}</td>
                            <td>{eachData.delta}</td>
                            <td>{eachData.createdOn}</td>
                        </tr>
                    )}
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    data: state.searchResult.data,
});

export default withRouter(connect(mapStateToProps, {getResult, getSearchResult})(Content));
