import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import {expect} from '../../service/chai';
import renderer from 'react-test-renderer';

import FilterStatus from './FilterStatus';


describe('FilterStatus', () => {
    test('should match snapshot', () => {
        const searchData = jest.fn();
        expect(toJson(shallow(<FilterStatus searchData={searchData}/>))).toMatchSnapshot();
    });
});
