import React from 'react';

const FilterStatus = ({searchData}) => {
    return (
        <tr>
            <th>filter by status</th>
            <td>
                <form onSubmit={searchData}>
                    <select name="filterByStatus">
                        <option value="">
                            select an value
                        </option>
                        <option value="COMPLETED">
                            completed
                        </option>
                        <option value="ERROR">
                            error
                        </option>
                        <option value="CANCELED">
                            canceled
                        </option>
                    </select>
                    <input type="submit" value="search"/>
                </form>
            </td>
        </tr>
    );
};

export default FilterStatus;