import React from 'react';

const SearchByName = ({searchData}) => {
    return (
        <tr>
            <th>search by name</th>
            <td>
                <form onSubmit={searchData}>
                    <input type="text" name="textSearch"/>
                    <input type="submit" value="search"/>
                </form>
            </td>
        </tr>
    );
};

export default SearchByName;