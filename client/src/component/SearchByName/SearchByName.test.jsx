import React from 'react';
import {shallow} from 'enzyme';
import {expect} from '../../service/chai';
import toJson from 'enzyme-to-json';

import SearchByName from "./SearchByName";

describe('searchByName', () => {
    test('should match snapshot', () => {
        const searchData = jest.fn();
        expect(toJson(shallow(<SearchByName searchData={searchData}/>))).toMatchSnapshot();
    });
});
