import React from 'react';
import {shallow} from 'enzyme';
import {expect} from '../../service/chai';
import toJson from 'enzyme-to-json';

import Pagination from './Pagination';


describe('Pagination', () => {
    test('should match snapshot', () => {
        const nextPage = jest.fn();
        const prevPage = jest.fn();
        const pageNumber = 1;
        expect(toJson(shallow(<Pagination nextPage={nextPage} prevPage={prevPage}
                                   pageNumber={pageNumber}/>))).toMatchSnapshot();
    });
});
