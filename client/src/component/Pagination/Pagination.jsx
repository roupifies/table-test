import React from 'react';

const Pagination = ({nextPage, prevPage, pageNumber}) => {
    return (
        <tr>
            <th>Pagination</th>
            <td>
                <input disabled={pageNumber === 0} type="button" value="prev"
                       onClick={prevPage}/>
                <input disabled={pageNumber === 24} type="button" value="next"
                       onClick={nextPage}/>
            </td>
        </tr>
    );
};

export default Pagination;