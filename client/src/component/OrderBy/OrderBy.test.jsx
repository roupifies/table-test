import React from 'react';
import { shallow } from 'enzyme';
import {expect} from '../../service/chai';
import toJson from 'enzyme-to-json';

import OrderBy from './OrderBy';


describe('OrderBy', () => {
    test('should match snapshot', () => {
        const fetchData = jest.fn();
        expect(toJson(shallow(<OrderBy fetchData={fetchData}/>))).toMatchSnapshot();
    });
});
