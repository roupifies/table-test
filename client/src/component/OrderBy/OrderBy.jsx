import React from 'react';

const OrderBy = ({fetchData}) => {
    return (
        <tr>
            <th>order by</th>
            <td>
                <form onSubmit={fetchData}>
                    <select name="orderBy">
                        <option value="id">
                            id
                        </option>
                        <option value="name">
                            name
                        </option>
                        <option value="createdOn">
                            createdOn
                        </option>
                    </select>
                    <input type="submit" value="search"/>
                </form>
            </td>
        </tr>
    );
};

export default OrderBy;