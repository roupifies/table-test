import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Content from '../Content/Content';
import store from '../../istore';
import './App.css';

const App = () => (
    <Provider store={store}>
        <BrowserRouter>
            <div className="App">
                <main>
                    <Switch>
                        <Route path="*" component={Content} />
                    </Switch>
                </main>
            </div>
        </BrowserRouter>
    </Provider>
);

export default App;
