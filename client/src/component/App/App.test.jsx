import React from 'react';
import {shallow} from 'enzyme';
import {expect} from '../../service/chai';

import App from './App';


describe('App', () => {
    test('should match snapshot', () => {
        expect(shallow(<App/>)).toMatchSnapshot();
    });
});
