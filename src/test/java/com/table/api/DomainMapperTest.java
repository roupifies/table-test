package com.table.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.table.api.common.DateUtil;
import com.table.api.domain.OrderBy;
import com.table.api.domain.response.DomainItem;
import com.table.api.domain.response.DomainResponse;
import com.table.api.ouband.response.OutbandItem;
import com.table.api.ouband.response.OutbandResponse;

@RunWith(MockitoJUnitRunner.class)
public class DomainMapperTest {

    private static final String ORDER_BY_ID = "id";
    private static final int FIRST_PAGE_NUMBER = 0;
    private static final String SOME_CREATION_DATE = "some/creation/date";
    private static final String SOME_EXPECTED_CREATION_DATE = "someExpected/creation/date";
    private static final int SOME_DELTA = 11;
    private static final String SOME_DESCRIPTION = "some description";
    private static final String SOME_NAME = "some name";
    private static final String SOME_STATUS = "some status";

    @InjectMocks
    private DomainMapper testSubject;

    @Mock
    private DateUtil dateUtil;

    @Test
    public void mapOutbandItemToDomainItemWithExpectedSize() {
        final int size = 100;
        final OutbandResponse outbandResponse = getOutbandResponses(SOME_CREATION_DATE, SOME_DELTA, SOME_DESCRIPTION, SOME_NAME, SOME_STATUS, size);

        when(dateUtil.convertToShortDate(any())).thenReturn(Optional.of(SOME_EXPECTED_CREATION_DATE));

        final DomainResponse result = testSubject.map(outbandResponse, ORDER_BY_ID, FIRST_PAGE_NUMBER);
        final List<DomainItem> domainItem = result.getOutput();

        final int expectedSize = 20;
        assertEquals(expectedSize, domainItem.size());
    }

    @Test
    public void mapOutbandItemToDomainItemWithToExpectedPageNumber() {
        final int size = 100;
        final int thirdPageNumber = 3;
        final OutbandResponse outbandResponse = getOutbandResponses(SOME_CREATION_DATE, SOME_DELTA, SOME_DESCRIPTION, SOME_NAME, SOME_STATUS, size);

        when(dateUtil.convertToShortDate(any())).thenReturn(Optional.of(SOME_EXPECTED_CREATION_DATE));

        final DomainResponse result = testSubject.map(outbandResponse, ORDER_BY_ID, thirdPageNumber);

        final int expectedResultId = 39;
        assertEquals(expectedResultId, result.getOutput().get(0).getId());
    }

    @Test
    public void mapOutbandItemToDomainItem() {
        final int size = 100;
        final OutbandResponse outbandResponse = getOutbandResponses(SOME_CREATION_DATE, SOME_DELTA, SOME_DESCRIPTION, SOME_NAME, SOME_STATUS, size);

        when(dateUtil.convertToShortDate(eq(89 + "_" + SOME_CREATION_DATE))).thenReturn(Optional.of(SOME_EXPECTED_CREATION_DATE));

        final DomainResponse result = testSubject.map(outbandResponse, ORDER_BY_ID, FIRST_PAGE_NUMBER);

        final int index = 10;
        final int someId = size - 1 - index;
        final DomainItem domainItem = result.getOutput().get(index);
        assertEquals(SOME_EXPECTED_CREATION_DATE, domainItem.getCreatedOn());
        assertEquals(someId + "_" + SOME_NAME, domainItem.getName());
        assertEquals(someId, domainItem.getId());
        assertEquals(someId + "_" + SOME_STATUS, domainItem.getStatus());
        assertEquals(SOME_DELTA, domainItem.getDelta());
        assertEquals(SOME_DESCRIPTION, domainItem.getDescription());
    }

    @Test
    public void mapOutbandItemToDomainItemOrderByName() {
        final int size = 100;
        final OutbandResponse outbandResponse = getOutbandResponses(SOME_CREATION_DATE, SOME_DELTA, SOME_DESCRIPTION, SOME_NAME, SOME_STATUS, size);

        when(dateUtil.convertToShortDate(any())).thenReturn(Optional.of(SOME_EXPECTED_CREATION_DATE));

        final DomainResponse result = testSubject.map(outbandResponse, OrderBy.name.name(), FIRST_PAGE_NUMBER);

        final DomainItem domainItem = result.getOutput().get(0);
        assertEquals(9 + "_" + SOME_NAME, domainItem.getName());
    }

    @Test
    public void mapOutbandItemToDomainItemToGetFilteredName() {
        final int size = 100;
        final OutbandResponse outbandResponse = getOutbandResponses(SOME_CREATION_DATE, SOME_DELTA, SOME_DESCRIPTION, SOME_NAME, SOME_STATUS, size);

        when(dateUtil.convertToShortDate(any())).thenReturn(Optional.of(SOME_EXPECTED_CREATION_DATE));

        final String anyStatus = "";
        final String somePageName = "11";
        final DomainResponse result = testSubject.map(outbandResponse, somePageName, anyStatus);

        final List<DomainItem> domainItem = result.getOutput();
        domainItem.forEach((item) -> assertTrue(item.getName().contains(somePageName)));
    }

    @Test
    public void mapOutbandItemToDomainItemToGetFilteredStatus() {
        final int size = 100;
        final OutbandResponse outbandResponse = getOutbandResponses(SOME_CREATION_DATE, SOME_DELTA, SOME_DESCRIPTION, SOME_NAME, SOME_STATUS, size);

        when(dateUtil.convertToShortDate(any())).thenReturn(Optional.of(SOME_EXPECTED_CREATION_DATE));

        final String anyPagePage = "";
        final String someStatus = "someStatus";
        final DomainResponse result = testSubject.map(outbandResponse, anyPagePage, someStatus);

        final List<DomainItem> domainItem = result.getOutput();
        domainItem.forEach((item) -> assertTrue(item.getStatus().contains(someStatus)));
    }

    @Test
    public void mapOutbandItemToDomainItemOrderByCreationDate() {
        final int size = 2;
        final OutbandResponse outbandResponse = getOutbandResponses(SOME_CREATION_DATE, SOME_DELTA, SOME_DESCRIPTION, SOME_NAME, SOME_STATUS, size);

        final String someEarlierDate = "11/10/2010";
        final String someLaterDate = "12/10/2010";
        when(dateUtil.convertToShortDate(eq(0 + "_" + SOME_CREATION_DATE))).thenReturn(Optional.of(someEarlierDate));
        when(dateUtil.convertToShortDate(eq(1 + "_" + SOME_CREATION_DATE))).thenReturn(Optional.of(someLaterDate));

        final DomainResponse result = testSubject.map(outbandResponse, OrderBy.name.name(), FIRST_PAGE_NUMBER);

        final DomainItem domainItem = result.getOutput().get(0);
        assertEquals(someLaterDate, domainItem.getCreatedOn());
    }

    private OutbandResponse getOutbandResponses(final String createdOn, final int someDelta,
                                                final String someDescription,
                                                final String someName, final String someStatus, final int size) {
        final OutbandResponse outbandResponse = new OutbandResponse();
        final OutbandItem[] output = new OutbandItem[size];
        for (int i = 0; i < size; i++) {
            output[i] = getOutbandItem(i + "_" + createdOn, someDelta, someDescription, i, i + "_" + someName, i + "_" + someStatus);
        }
        outbandResponse.setOutput(output);
        return outbandResponse;
    }

    private OutbandItem getOutbandItem(final String createdOn, final int someDelta, final String someDescription, final int someId, final String someName,
                                       final String someStatus) {
        final OutbandItem outbandItem = new OutbandItem();
        outbandItem.setCreatedOn(createdOn);
        outbandItem.setDelta(someDelta);
        outbandItem.setDescription(someDescription);
        outbandItem.setId(someId);
        outbandItem.setName(someName);
        outbandItem.setStatus(someStatus);
        return outbandItem;
    }
}