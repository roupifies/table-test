package com.table.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.table.api.common.RestClient;
import com.table.api.domain.response.DomainResponse;
import com.table.api.ouband.RequestUrl;
import com.table.api.ouband.response.OutbandResponse;

@RunWith(MockitoJUnitRunner.class)
public class TableServiceImplTest {

    private static final String ANY_ORDER_BY = "any";
    private static final int ANY_PAGE_NUMBER = 1;
    public static final String ANY_NAME = "any name";
    public static final String ANY_STATUS = "any status";

    @InjectMocks
    private TableServiceImpl testSubject;

    @Mock
    private RestClient restClient;
    @Mock
    private RequestUrl requestUrl;
    @Mock
    private DomainMapper domainMapper;

    @Before
    public void setUp() {
        mockDomainMapper();
    }

    @Test
    public void invokeGetRequestUrlOnFetchData() {
        mockRestClient(new OutbandResponse());

        testSubject.fetchData(ANY_ORDER_BY, ANY_PAGE_NUMBER);

        verify(requestUrl).get();
    }

    @Test
    public void invokeRestClientOnFetchDataWithExpectedUrl() {
        final String someUrl = "http://some.url";
        mockRestClient(new OutbandResponse());
        when(requestUrl.get()).thenReturn(someUrl);

        testSubject.fetchData(ANY_ORDER_BY, ANY_PAGE_NUMBER);

        verify(restClient).get(eq(someUrl), eq(OutbandResponse.class));
    }

    @Test
    public void invokeDomainMapperOnFetchDataWithExpectedParams() {
        final String someUrl = "http://some.url";
        final OutbandResponse someOutbandResponse = new OutbandResponse();
        mockRestClient(someOutbandResponse);
        when(requestUrl.get()).thenReturn(someUrl);

        final String someOrderBy = "some order by";
        final int somePageNumber = 10;
        testSubject.fetchData(someOrderBy, somePageNumber);

        verify(domainMapper).map(eq(someOutbandResponse), eq(someOrderBy), eq(somePageNumber));
    }

    @Test
    public void invokeDomainMapperOnSearchDataWithExpectedParams() {
        final String someUrl = "http://some.url";
        final OutbandResponse someOutbandResponse = new OutbandResponse();
        mockRestClient(someOutbandResponse);
        when(requestUrl.get()).thenReturn(someUrl);

        final String someName = "some name";
        final String someStatus = "some status";
        testSubject.searchData(someName, someStatus);

        verify(domainMapper).map(eq(someOutbandResponse), eq(someName), eq(someStatus));
    }

    @Test
    public void invokeGetRequestUrlOnSearchData() {
        mockRestClient(new OutbandResponse());

        testSubject.searchData(ANY_NAME, ANY_STATUS);

        verify(requestUrl).get();
    }

    @Test
    public void invokeRestClientOnSearchDataWithExpectedUrl() {
        final String someUrl = "http://some.url";
        mockRestClient(new OutbandResponse());
        when(requestUrl.get()).thenReturn(someUrl);

        testSubject.searchData(ANY_NAME, ANY_STATUS);

        verify(restClient).get(eq(someUrl), eq(OutbandResponse.class));
    }

    private void mockDomainMapper() {
        when(domainMapper.map(any(), anyString(), any())).thenReturn(new DomainResponse());
    }

    private void mockRestClient(final OutbandResponse body) {
        final ResponseEntity<OutbandResponse> responseResponseEntity = ResponseEntity.ok(body);
        when(restClient.get(any(), eq(OutbandResponse.class))).thenReturn(responseResponseEntity);
    }

}