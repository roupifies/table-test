package com.table.api.ouband;

import static org.assertj.core.api.Java6Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RequestUrlTest {

    @InjectMocks
    private RequestUrl testSubject;

    private static final String SOME_URL = "http://someUrl.com";
    private static final String SOME_PATH = "king-airnd-recruitment-sandbox-data/data.json";

    @Test
    public void returnExpectedSearchUrl() {
        // Given
        final String expectedSearchResult = SOME_URL.concat("/").concat(SOME_PATH);
        testSubject.setApiUrl(SOME_URL);

        // When
        final String result = testSubject.get();

        // Then
        assertThat(result).isEqualTo(expectedSearchResult);
    }

}
