package com.table.api.common;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DateUtilTest {

    private DateUtil testSubject = new DateUtil();

    @DataProvider
    private Object[][] dateTypes() {
        return new Object[][] {
            {"1532501259000", "25/07/2018"},
            {"1528723829000", "11/06/2018"},
            {"2018-07-10T09:03:21.000Z", "18/01/1970"},
            {"2018-06-11T14:30:02.000ZZ", "18/01/1970"}
        };
    }

    @DataProvider
    private Object[][] invalidDateTypes() {
        return new Object[][] {
            {"", Optional.empty()},
            {"some-non-valid-date", Optional.empty()},
            {null, Optional.empty()}
        };
    }

    @DataProvider
    private Object[][] compareDates() {
        return new Object[][] {
            {"11/10/2010", "12/10/2010", -1},
            {"11/11/2010", "12/10/2010", 1},
            {"some-not-valid-date", "2018-07-12T09:03:21.000Z", -1},
            {"2018-07-12T09:03:21.000Z", "", -1},
            {"some-not-valid-date", "", -1},
            {null, "", -1},
        };
    }

    @Test(dataProvider = "dateTypes")
    public void convertGivenDateToShortDate(final String givenDate, final String expectedDate) {
        final Optional<String> result = testSubject.convertToShortDate(givenDate);

        assertEquals(result.get(), expectedDate);
    }

    @Test(dataProvider = "invalidDateTypes")
    public void returnEmptyOptionalForNonValidDate(final String givenDate, final Optional expectedDate) {
        final Optional<String> result = testSubject.convertToShortDate(givenDate);
        assertEquals(result, expectedDate);
    }

    @Test(dataProvider = "compareDates")
    public void compareDates(final String firstDate, final String secondDate, final int expectedResult) {
        final int result = testSubject.compare(firstDate, secondDate);
        assertEquals(result, expectedResult);
    }
}
