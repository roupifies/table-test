package com.table.api.common;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import com.table.api.ouband.response.OutbandResponse;

@RunWith(MockitoJUnitRunner.class)
public class RestClientTest {

    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void invokeRestTemplateWithGivenUrlAndResponseType() {
        //Given
        when(restTemplateBuilder.build()).thenReturn(restTemplate);
        final String someUrl = "http://some.url";
        final Class<OutbandResponse> someResponseType = OutbandResponse.class;

        //When
        new RestClient(restTemplateBuilder).get(someUrl, someResponseType);

        //Then
        verify(restTemplate).getForEntity(eq(someUrl), eq(someResponseType));
    }
}