package com.table;

import com.table.api.domain.response.DomainResponse;

public interface TableService {
    DomainResponse fetchData(final String ordered, final int pageNumber);
    DomainResponse searchData(final String s, final String name);
}
