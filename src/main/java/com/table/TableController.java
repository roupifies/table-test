package com.table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

import com.table.api.domain.response.DomainResponse;

@Controller
public class TableController {

    private final TableService tableService;

    @Autowired
    public TableController(final TableService tableService) {
        this.tableService = tableService;
    }

    @GetMapping({"/", "/data"})
    @CrossOrigin(origins = "http://localhost:3000")
    public String fetchData(Model model,
                            @RequestParam(value = "orderBy", required = false, defaultValue = "id") final String orderBy,
                            @RequestParam(value = "pageNumber", required = false, defaultValue = "0") final int pageNumber) {
        final DomainResponse items = tableService.fetchData(orderBy, pageNumber);
        model.addAttribute("data", new Gson().toJson(items));
        return "data";
    }

    @GetMapping({"/search", "/search"})
    @CrossOrigin(origins = "http://localhost:3000")
    public String searchData(Model model, @RequestParam(value = "name", required = false, defaultValue = "") final String name,  @RequestParam(value = "status", required = false, defaultValue = "") final String status) {
        final DomainResponse items = tableService.searchData(name, status);
        model.addAttribute("data", new Gson().toJson(items));
        return "search";
    }
}
