package com.table.api.ouband.response;

public class OutbandResponse {

    private OutbandItem[] output;

    public OutbandResponse() {
    }

    public OutbandItem[] getOutput() {
        return output;
    }

    public void setOutput(final OutbandItem[] output) {
        this.output = output;
    }
}
