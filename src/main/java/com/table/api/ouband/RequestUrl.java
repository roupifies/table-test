package com.table.api.ouband;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class RequestUrl {

    private static final String PATH = "king-airnd-recruitment-sandbox-data/data.json";

    @Value("${api.url}")
    private String apiUrl;

    String getApiUrl() {
        return apiUrl;
    }

    void setApiUrl(final String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String get() {
        return UriComponentsBuilder.fromHttpUrl(apiUrl).path(PATH).encode().toUriString();
    }


}
