package com.table.api.domain.response;

import java.util.List;

public class DomainResponse {

    private List<DomainItem> output;

    public DomainResponse() {
    }

    public List<DomainItem> getOutput() {
        return output;
    }

    public void setOutput(final List<DomainItem> output) {
        this.output = output;
    }
}
