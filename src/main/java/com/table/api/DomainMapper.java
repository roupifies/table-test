package com.table.api;

import static com.table.api.domain.OrderBy.createOn;
import static com.table.api.domain.OrderBy.name;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.table.api.common.DateUtil;
import com.table.api.domain.response.DomainItem;
import com.table.api.domain.response.DomainResponse;
import com.table.api.ouband.response.OutbandItem;
import com.table.api.ouband.response.OutbandResponse;

@Component
public class DomainMapper {

    private static final int PAGE_SIZE = 20;
    private DateUtil dateUtil;

    @Autowired
    private DomainMapper(final DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    public DomainResponse map(final OutbandResponse outbandResponse, final String orderBy, final int pageNumber) {
        final DomainResponse domainResponse = new DomainResponse();
        final List<DomainItem> domainItems = Arrays.stream(outbandResponse.getOutput())
            .map(this::mapItem)
            .sorted(getComparator(orderBy)).skip(pageNumber * PAGE_SIZE).limit(PAGE_SIZE)
            .collect(Collectors.toList());
        domainResponse.setOutput(domainItems);
        return domainResponse;
    }

    public DomainResponse map(final OutbandResponse outbandResponse, final String name, final String status) {
        final DomainResponse domainResponse = new DomainResponse();
        final List<DomainItem> domainItems = Arrays.stream(outbandResponse.getOutput())
            .map(this::mapItem)
            .filter(item -> item.getName().contains(name))
            .filter(domainItem -> domainItem.getStatus().contains(status))
            .limit(PAGE_SIZE).collect(Collectors.toList());
        domainResponse.setOutput(domainItems);
        return domainResponse;
    }

    private DomainItem mapItem(final OutbandItem outputOutbandItem) {
        final DomainItem domainItem = new DomainItem();
        domainItem.setDelta(outputOutbandItem.getDelta());
        domainItem.setDescription(outputOutbandItem.getDescription());
        domainItem.setId(outputOutbandItem.getId());
        domainItem.setName(outputOutbandItem.getName());
        domainItem.setStatus(outputOutbandItem.getStatus());
        dateUtil.convertToShortDate(outputOutbandItem.getCreatedOn()).ifPresent(domainItem::setCreatedOn);
        return domainItem;
    }

    private Comparator<DomainItem> getComparator(final String orderBy) {
        if (orderBy.equals(createOn.name())) {
            return (o1, o2) -> dateUtil.compare(o1.getCreatedOn(), o2.getCreatedOn());
        } else if (orderBy.equals(name.name())) {
            return Comparator.comparing(DomainItem::getName).reversed();
        } else {
            return Comparator.comparingInt(DomainItem::getId).reversed();
        }
    }
}
