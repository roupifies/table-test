package com.table.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.table.TableService;
import com.table.api.common.RestClient;
import com.table.api.domain.response.DomainResponse;
import com.table.api.ouband.RequestUrl;
import com.table.api.ouband.response.OutbandResponse;

@Component
class TableServiceImpl implements TableService {

    private RestClient restClient;
    private RequestUrl requestUrl;
    private DomainMapper domainMapper;

    @Autowired
    private TableServiceImpl(final RestClient restClient, final RequestUrl requestUrl, final DomainMapper domainMapper) {
        this.restClient = restClient;
        this.requestUrl = requestUrl;
        this.domainMapper = domainMapper;
    }

    @Override
    public DomainResponse fetchData(final String orderBy, final int pageNumber) {
        final OutbandResponse outbandResponse = getOutput();
        return domainMapper.map(outbandResponse, orderBy, pageNumber);
    }

    @Override public DomainResponse searchData(final String name, final String status) {
        final OutbandResponse outbandResponse = getOutput();
        return domainMapper.map(outbandResponse, name, status);
    }

    private OutbandResponse getOutput() {
        final String requestUrl = this.requestUrl.get();
        final ResponseEntity<OutbandResponse> outbandResponseResponseEntity = restClient.get(requestUrl, OutbandResponse.class);
        return outbandResponseResponseEntity.getBody();
    }
}
