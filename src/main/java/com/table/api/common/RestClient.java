package com.table.api.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestClient {

	private RestTemplate restTemplate;

	@Autowired RestClient(final RestTemplateBuilder restTemplateBuilder){
		this.restTemplate = restTemplateBuilder.build();
	}

	public <T> ResponseEntity<T> get(final String url, final Class<T> responseType) {
		return restTemplate.getForEntity(url, responseType);
	}
}
