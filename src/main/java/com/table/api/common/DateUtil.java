package com.table.api.common;

import java.text.SimpleDateFormat;
import java.util.Optional;

import org.springframework.stereotype.Component;

@Component
public class DateUtil {

    private static final String DATE_FORMATTER = "dd/MM/yyyy";
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_FORMATTER);
    private static final String LONG_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static final SimpleDateFormat LONG_DATE_FORMATTER = new SimpleDateFormat(LONG_DATE_FORMAT);

    public Optional<String> convertToShortDate(final String date) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMATTER);
        return getDateAsEpoch(date).map(simpleDateFormat::format);
    }

    public int compare(final String firstDate, final String secondDate) {
        try {
            return SIMPLE_DATE_FORMAT.parse(firstDate).compareTo(SIMPLE_DATE_FORMAT.parse(secondDate));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    private Optional<Long> getDateAsEpoch(final String date) {
        try {
            if (date.contains("-")) {
                return Optional.of(LONG_DATE_FORMATTER.parse(date).getTime() / 1000);
            } else {
                return Optional.of(Long.valueOf(date));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
