package com.table.api;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

@ControllerAdvice
public class AdviceController {

  private final Logger logger = LogManager.getLogger(AdviceController.class);

  @ExceptionHandler( { RestClientException.class, HttpClientErrorException.class })
  public ResponseEntity<List<String>> restClientException(final RestClientException e) {
    logger.error(e.getMessage(), e);
    return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
  }

}
